## Create a new project named "demo-api"
1. Create a folder with the name "demo-api"
2. Open the terminal and navigate into that folder
3. Run the following commands:
	- ```npm init -y```
	- ```npm i -S dotenv```
	- ```npm i -S koa```
	- ```npm i -S koa-body```
	- ```npm i -S koa-router```
	- ```npm i -S mongoose```
	- ```npm i -D nodemon```

4. Create a file named ".gitignore"
	- add the content of https://github.com/github/gitignore/blob/master/Node.gitignore 
5. Create a .env file and copy the following content inside the folder
	```
	PORT=3000
	# Mongo
	MONGO_DB_HOST=localhost
	MONGO_BD_PORT=27017
	MONGO_DB_NAME=test
	```

6. Create a folder named "src"
7. Within src folder create the following:
	- A file named "server.js"
	- A folder named "api"
	- Within api folder create a index.js file
	- Check the directory structure:
		```
		node_modules
		src
		  | api
		    | index.js
		  | server.js
		.env
		.gitignore
		package.json
		```

8. Add the following content to *src/server.js* file
	```
	require('dotenv').config()

	const Koa = require('koa')
	const mongoose = require('mongoose')
	const koaBody = require('koa-body')

	const searchParams = require('./middleware/searchParams')
	const api = require('./api')

	const {
		PORT = 3000,
		MONGO_DB_HOST,
		MONGO_BD_PORT,
		MONGO_DB_NAME
	} = process.env

	mongoose.connect(
		`mongodb://${ MONGO_DB_HOST }:${ MONGO_BD_PORT }/${ MONGO_DB_NAME }`,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
	mongoose.connection.on('error', console.error);

	const app = new Koa()

	app
		.use(koaBody())
		.use(api.allowedMethods())
		.use(api.routes())

		.listen(PORT, () => {
			console.log('Server started on port', PORT)
		})
	```

9. Add the following content to the file *src/api/index.js*
	```
	const Router = require('koa-router')

	const posts = require('./posts')
	const users = require('./users')

	const api = new Router({
		prefix: '/api'
	})
	api.get('/', (ctx) => { ctx.body = 'ok' })

	module.exports = api
	```

10. Modify the package.json file and change the *scripts* entry to the following:
	```
	"scripts": {
		"dev": "nodemon src/server.js"
	},
	```
11. In the terminal run: ```npm run dev```
	- The server should start at **PORT** 3000
	- Open the url *http://localhost:3000* on a browser
		- You should should see the message "Not Found"
	- Change the url to *http://localhost:3000/api*
		- Now the message should be "ok"
	- If so, everything is working
		- Note: Mongo must be running and there must be a database called "test"

## Create the CRUD routes and Controller for 
1. Create a folder within the *api* folder named "posts"
	- The content of the folder must be as shown bellow
		```
		src
		  | api
		    | posts
		      | controller.js
	          | index.js
		      | model.js
		```
	- Put the following content on *model.js* file:
		```
		const mongoose = require('mongoose')

		const { Schema } = mongoose

		const postSchema = new Schema({
			title: {
				type: String,
				required: true
			},
			summary: {
				type: String
			},
			updated: {
				type: Date,
				default: Date.now
			}
		})

		module.exports = mongoose.model('Post', postSchema)
		```
	- Copy the next code the *controller.js* file
		```
		const Post = require('./model')

		class PostsController {

			/**
			* Get all posts
			* @param {ctx} Koa Context
			*/
			async find(ctx) {
				ctx.body = await Post.find()
			}

			/**
			* Find a post
			* @param {ctx} Koa Context
			*/
			async findById(ctx) {
				try {
					const post = await Post.findById(ctx.params.id)
					if (!post) {
						ctx.throw(404)
					}
					ctx.body = post
				} catch (err) {
					if (err.name === 'CastError' || err.name === 'NotFoundError') {
						ctx.throw(404)
					}
					ctx.throw(500)
				}
			}

			/**
			* Add a post
			* @param {ctx} Koa Context
			*/
			async add(ctx) {
				try {
					const post = await new Post(ctx.request.body).save()
					ctx.set('Location', `/posts/${ post._id }`)
					ctx.status = 201
					ctx.body = post
				} catch (err) {
					ctx.throw(422)
				}
			}

			/**
			* Update a post
			* @param {ctx} Koa Context
			*/
			async update(ctx) {
				try {
					const post = await Post.findByIdAndUpdate(
						ctx.params.id,
						ctx.request.body,
						{
							new: true,
							useFindAndModify: false
						}
					)
					if (!post) {
						ctx.throw(404)
					}
					// ctx.status = 204 - 204 is No Content
					ctx.body = post
				} catch (err) {
					console.log('err', err)
					if (err.name === 'CastError' || err.name === 'NotFoundError') {
						ctx.throw(404)
					}
					ctx.throw(500)
				}
			}

			/**
			* Delete a post
			* @param {ctx} Koa Context
			*/
			async delete(ctx) {
				try {
					const post = await Post.findByIdAndRemove(ctx.params.id)
					if (!post) {
						ctx.throw(404)
					}
					ctx.body = post
				} catch (err) {
					if (err.name === 'CastError' || err.name === 'NotFoundError') {
						ctx.throw(404)
					}
					ctx.throw(500)
				}
			}
		}

		module.exports = new PostsController()
		```
	- Finally add the following code to the index.js
		```
		const Router = require('koa-router')

		const Controller = require('./controller')

		const router = new Router({
			prefix: '/posts'
		})

		// GET /api/posts
		router.get('/', Controller.find)

		// POST /api/posts
		router.post('/', Controller.add)

		// GET /api/posts/id
		router.get('/:id', Controller.findById)

		// PUT /api/posts/id
		router.put('/:id', Controller.update)

		// DELETE /api/posts/id
		router.delete('/:id', Controller.delete)

		module.exports = router
		```
	- Import and Register the posts routes to the API router:
		```
		const posts = require('./posts')
		...
		api.use(posts.allowedMethods())
		api.use(posts.routes())
		```
	- Use the application Postman to Create,Read,Update and Delete posts

2. Extend the route */api/posts* the handle sort mechanism provided by search params:
	- The route should handle the following urls and respond accordingly
		- *http://localhost:3000/api/posts?sort=title,asc*
		- *http://localhost:3000/api/posts?sort=title,desc*
	- Create a middleware named "searchParams"
		- The middleware should parse the search params and add the result to *ctx.searchParams* (ctx is the koa context)
		- register the koa middleware as follows (in *server.js* file)
			```
			const searchParams = require('./middleware/searchParams')
			...
			app.use(searchParams())
			```

## Extend the existing API

1. Create an Author Model, Controller an Route (CRUD) so that it can be associated an existing user as the owner of the post (One-to-One Relationship)
	- A simple User Schema should look like this:
		```
		const mongoose = require('mongoose')

		const { Schema } = mongoose

		const userSchema = new Schema({
			name: {
				type: String,
				required: true
			},
			updated: {
				type: Date,
				default: Date.now
			}
		})

		module.exports = mongoose.model('User', userSchema)
		```
	- The Post model should now look like:
		```
		const mongoose = require('mongoose')

		const { Schema } = mongoose

		const postSchema = new Schema({
			title: {
				type: String,
				required: true
			},
			summary: {
				type: String
			},
			updated: {
				type: Date,
				default: Date.now
			},
			author: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User'
			}
		})

		module.exports = mongoose.model('Post', postSchema)
		```
	- When creating/updating a post we should be able to associate an existing user

2. Create an Comment Model so that it can be added to a list of comments within a post (One-to-Many Relationship)
	- The Comment model should have
		```
		const mongoose = require('mongoose')

		const { Schema } = mongoose

		const commentSchema = new Schema({
			postId: Schema.Types.ObjectId,
			author: {
				type: Schema.Types.ObjectId,
				ref: 'User'
			},
			content: String,
			votes: {
				type: Number,
				default: 0
			},
			updated: {
				type: Date,
				default: Date.now
			}
		})

		module.exports = mongoose.model('Comment', commentSchema)
		```
	- Submit a POST to:
		- http://localhost:3000/api/posts/<post_id>/comments
		- with the JSON body:
			```
			{
				"author": <user_id>,
				"content": "Nothing to say about this post"
			}
			```
		- Should create a new Comment referencing the **post ID** passed trough the URL parameter (if the post exists)
		- If the post exists, it should update the post by appending it to the comments list of the matching post.
		- When getting the post the result should look like the following:
			```
			{
				"comments": [
					{
						"votes": 0,
						"_id": "5db29c9b09a2b8a8af30ca83",
						"postId": "5db23b3695dc12a54b72377a",
						"author": "5db292e3f61f87a7db9fd3f7",
						"content": "Nothing to say about this post",
						"updated": "2019-10-25T06:56:27.313Z",
						"__v": 0
					}
				],
				"_id": "5db23b3695dc12a54b72377a",
				"title": "aabcd title",
				"summary": "aabc summary",
				"updated": "2019-10-25T00:00:54.655Z",
				"__v": 1,
				"author": {
					"_id": "5db292e3f61f87a7db9fd3f7",
					"name": "Edgar Esteves",
					"updated": "2019-10-25T06:14:59.853Z",
					"__v": 0
				}
			}
			```
