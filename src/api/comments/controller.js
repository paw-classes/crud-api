const Comment = require('./model')
const Post = require('../posts/model')

class CommentsController {

	/**
	 * Add a post
	 * @param {ctx} Koa Context
	 */
	async add(ctx) {
		try {
			const { postId } = ctx.params
			const post = await Post.findById(postId)
			if (post) {
				const comment = await new Comment({
					postId,
					...ctx.request.body,
				}).save()
				post.comments = [
					...(post.comments || []),
					comment._id
				]
				await post.save()
				ctx.set('Location', `/posts/${ post._id }`)
				ctx.status = 201
				ctx.body = comment
			}
		} catch (err) {
			ctx.throw(422)
		}
	}
}

module.exports = new CommentsController()
