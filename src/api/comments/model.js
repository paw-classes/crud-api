const mongoose = require('mongoose')

const { Schema } = mongoose

const commentSchema = new Schema({
	postId: Schema.Types.ObjectId,
	author: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	content: String,
	votes: {
		type: Number,
		default: 0
	},
	updated: {
		type: Date,
		default: Date.now
	}
})

module.exports = mongoose.model('Comment', commentSchema)