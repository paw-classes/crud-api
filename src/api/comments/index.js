const Router = require('koa-router')

const Controller = require('./controller')

const router = new Router()

// POST /api/posts/<post_id>/comments
router.post('/:postId/comments', Controller.add)

module.exports = router