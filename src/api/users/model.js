const mongoose = require('mongoose')

const { Schema } = mongoose

const userSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	updated: {
		type: Date,
		default: Date.now
	}
})

module.exports = mongoose.model('User', userSchema)