const mongoose = require('mongoose')

require('../comments/model')

const { Schema } = mongoose

const postSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	summary: {
		type: String
	},
	updated: {
		type: Date,
		default: Date.now
	},
	author: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	comments: [{
		type: Schema.Types.ObjectId,
		ref: 'Comment'
	}]
})

module.exports = mongoose.model('Post', postSchema)