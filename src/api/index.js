const Router = require('koa-router')

const posts = require('./posts')
const users = require('./users')

const api = new Router({
	prefix: '/api'
})
api.get('/', (ctx) => { ctx.body = 'ok' })
api.use(posts.allowedMethods())
api.use(posts.routes())
api.use(users.allowedMethods())
api.use(users.routes())

module.exports = api