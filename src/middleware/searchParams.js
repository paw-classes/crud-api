const { parse } = require('querystring')

const searchParams = () => async (ctx, next) => {
	const searchParams = parse(ctx.search.slice(1))
	ctx.searchParams = searchParams
	await next()
}

module.exports = searchParams